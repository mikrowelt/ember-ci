export default {
  firewallStatus: ['pending', 'running', 'accepted', 'rejected'],
  buildStatus: ['pending', 'running', 'succeed', 'fail'],
  buildInfoStatus: ['pending', 'running', 'success', 'failed'],
  colorClasses: {
    isGray: 'gray',
    isBlue: 'blue',
    isGreen: 'green',
    isRed: 'red'
  }
};
