import {faker} from 'ember-cli-mirage';

export default [
  {
    id: 1,
    build: 1,
    testValue: faker.random.number(99),
    testStatus: faker.random.number(2),
    securityValue: faker.random.number(99),
    securityStatus: faker.random.number(2),
    maintainabilityValue: faker.random.number(99),
    maintainabilityStatus: faker.random.number(2),
    workmanshipValue: faker.random.number(99),
    workmanshipStatus: faker.random.number(2),
    status: faker.random.number(2)
  },
  {
    id: 2,
    build: 2,
    testValue: faker.random.number(99),
    testStatus: faker.random.number(2),
    securityValue: faker.random.number(99),
    securityStatus: faker.random.number(2),
    maintainabilityValue: faker.random.number(99),
    maintainabilityStatus: faker.random.number(2),
    workmanshipValue: faker.random.number(99),
    workmanshipStatus: faker.random.number(2),
    status: faker.random.number(2)
  },
  {
    id: 3,
    build: 3,
    testValue: faker.random.number(99),
    testStatus: faker.random.number(2),
    securityValue: faker.random.number(99),
    securityStatus: faker.random.number(2),
    maintainabilityValue: faker.random.number(99),
    maintainabilityStatus: faker.random.number(2),
    workmanshipValue: faker.random.number(99),
    workmanshipStatus: faker.random.number(2),
    status: faker.random.number(2)
  },
  {
    id: 4,
    build: 4,
    testValue: faker.random.number(99),
    testStatus: faker.random.number(2),
    securityValue: faker.random.number(99),
    securityStatus: faker.random.number(2),
    maintainabilityValue: faker.random.number(99),
    maintainabilityStatus: faker.random.number(2),
    workmanshipValue: faker.random.number(99),
    workmanshipStatus: faker.random.number(2),
    status: faker.random.number(2)
  },
  {
    id: 5,
    build: 5,
    testValue: faker.random.number(99),
    testStatus: faker.random.number(2),
    securityValue: faker.random.number(99),
    securityStatus: faker.random.number(2),
    maintainabilityValue: faker.random.number(99),
    maintainabilityStatus: faker.random.number(2),
    workmanshipValue: faker.random.number(99),
    workmanshipStatus: faker.random.number(2),
    status: faker.random.number(2)
  },
  {
    id: 6,
    build: 6,
    testValue: faker.random.number(99),
    testStatus: faker.random.number(2),
    securityValue: faker.random.number(99),
    securityStatus: faker.random.number(2),
    maintainabilityValue: faker.random.number(99),
    maintainabilityStatus: faker.random.number(2),
    workmanshipValue: faker.random.number(99),
    workmanshipStatus: faker.random.number(2),
    status: faker.random.number(2)
  }
];
