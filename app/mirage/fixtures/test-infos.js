import {faker} from 'ember-cli-mirage';

export default [
  {
    id: 1,
    type: 'unit',
    passedTests: faker.random.number(),
    failedTests: faker.random.number(),
    codeCoverage: faker.random.number(100),
    status: faker.random.number(3),
    build: 1
  },
  {
    id: 2,
    type: 'functional',
    passedTests: faker.random.number(),
    failedTests: faker.random.number(),
    codeCoverage: faker.random.number(100),
    status: faker.random.number(3),
    build: 1
  },
  {
    id: 3,
    type: 'unit',
    passedTests: faker.random.number(),
    failedTests: faker.random.number(),
    codeCoverage: faker.random.number(100),
    status: faker.random.number(3),
    build: 2
  },
  {
    id: 4,
    type: 'functional',
    passedTests: faker.random.number(),
    failedTests: faker.random.number(),
    codeCoverage: faker.random.number(100),
    status: faker.random.number(3),
    build: 2
  },
  {
    id: 5,
    type: 'unit',
    passedTests: faker.random.number(),
    failedTests: faker.random.number(),
    codeCoverage: faker.random.number(100),
    status: faker.random.number(3),
    build: 3
  },
  {
    id: 6,
    type: 'functional',
    passedTests: faker.random.number(),
    failedTests: faker.random.number(),
    codeCoverage: faker.random.number(100),
    status: faker.random.number(3),
    build: 3
  },
  {
    id: 7,
    type: 'unit',
    passedTests: faker.random.number(),
    failedTests: faker.random.number(),
    codeCoverage: faker.random.number(100),
    status: faker.random.number(3),
    build: 4
  },
  {
    id: 8,
    type: 'functional',
    passedTests: faker.random.number(),
    failedTests: faker.random.number(),
    codeCoverage: faker.random.number(100),
    status: faker.random.number(3),
    build: 4
  },
  {
    id: 9,
    type: 'unit',
    passedTests: faker.random.number(),
    failedTests: faker.random.number(),
    codeCoverage: faker.random.number(100),
    status: faker.random.number(3),
    build: 5
  },
  {
    id: 10,
    type: 'functional',
    passedTests: faker.random.number(),
    failedTests: faker.random.number(),
    codeCoverage: faker.random.number(100),
    status: faker.random.number(3),
    build: 5
  },
  {
    id: 11,
    type: 'unit',
    passedTests: faker.random.number(),
    failedTests: faker.random.number(),
    codeCoverage: faker.random.number(100),
    status: faker.random.number(3),
    build: 6
  },
  {
    id: 12,
    type: 'functional',
    passedTests: faker.random.number(),
    failedTests: faker.random.number(),
    codeCoverage: faker.random.number(100),
    status: faker.random.number(3),
    build: 6
  }
];
