import {faker} from 'ember-cli-mirage';

export default [
  {
    id: 1,
    build: 1,
    buildAt: faker.date.past(),
    status: 0
  },
  {
    id: 2,
    build: 2,
    buildAt: faker.date.past(),
    status: 1
  },
  {
    id: 3,
    build: 3,
    buildAt: faker.date.past(),
    status: 0
  },
  {
    id: 4,
    build: 4,
    buildAt: faker.date.past(),
    status: 2
  },
  {
    id: 5,
    build: 5,
    buildAt: faker.date.past(),
    status: 3
  },
  {
    id: 6,
    build: 6,
    buildAt: faker.date.past(),
    status: 3
  }
];
