export default [

  {
    id: 1,
    title: 'Zeus_45',
    owner: null,
    type: 'build',
    currentBuild: 1
  },
  {
    id: 2,
    title: 'TheWall_1',
    owner: 'bart',
    type: 'firewall',
    currentBuild: 2
  },
  {
    id: 3,
    title: 'Stone_3',
    owner: 'homer',
    type: 'firewall',
    currentBuild: 3
  },
  {
    id: 4,
    title: 'Apollo_13',
    owner: null,
    type: 'build',
    currentBuild: 4
  },
  {
    id: 5,
    title: 'Vesta_3',
    owner: null,
    type: 'build',
    currentBuild: 5
  },
  {
    id: 6,
    title: 'Stone_1',
    owner: 'lisa',
    type: 'firewall',
    currentBuild: 6
  }

];
