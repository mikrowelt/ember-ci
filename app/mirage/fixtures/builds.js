import {faker} from 'ember-cli-mirage';

export default [
  {
    id: 1,
    target: 1,
    startedAt: faker.date.past(),
    status: faker.random.number(3),
    metrics: 1,
    buildInfo: 1,
    unitTest: 1,
    functionalTest: 2
  },
  {
    id: 2,
    target: 2,
    startedAt: faker.date.past(),
    status: faker.random.number(3),
    metrics: 2,
    buildInfo: 2,
    unitTest: 3,
    functionalTest: 4
  },
  {
    id: 3,
    target: 3,
    startedAt: faker.date.past(),
    status: faker.random.number(3),
    metrics: 3,
    buildInfo: 3,
    unitTest: 5,
    functionalTest: 6
  },
  {
    id: 4,
    target: 4,
    startedAt: faker.date.past(),
    status: faker.random.number(3),
    metrics: 4,
    buildInfo: 4,
    unitTest: 7,
    functionalTest: 8
  },
  {
    id: 5,
    target: 5,
    startedAt: faker.date.past(),
    status: faker.random.number(3),
    metrics: 5,
    buildInfo: 5,
    unitTest: 9,
    functionalTest: 10
  },
  {
    id: 6,
    target: 6,
    startedAt: faker.date.past(),
    status: faker.random.number(3),
    metrics: 6,
    buildInfo: 6,
    unitTest: 11,
    functionalTest: 12
  }

];
