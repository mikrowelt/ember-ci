export default function() {

  this.get('/targets', ['targets', 'builds', 'metrics', 'build-infos', 'test-infos']);

}
