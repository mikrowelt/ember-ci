import Ember from 'ember';
import InfoBox from './info-box';

export default InfoBox.extend({

  infoTemplate: 'info-boxes/build-info-box',

  title: 'Build'

});
