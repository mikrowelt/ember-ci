import Ember from 'ember';
import StatusColored from './status-colored';

export default StatusColored.extend({

  layoutName: 'components/info-box',

  classNames: ['info-box'],

  status: Ember.computed.reads('model.status'),

  classNameBindings: [
    'isGray:info-box_gray',
    'isBlue:info-box_blue',
    'isGreen:info-box_red',
    'isRed:info-box_green'
  ],

  title: null,

  click () {
    //this.showInfo();
  }

});
