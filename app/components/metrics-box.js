import Ember from 'ember';
import InfoBox from './info-box';

export default InfoBox.extend({

  infoTemplate: 'info-boxes/metrics-box',

  title: 'Metrics',

  testClass: (function () {
    return this.getArrowClass(this.get('model.testStatus'));
  }).property('model'),

  maintainabilityClass: (function () {
    return this.getArrowClass(this.get('model.maintainabilityStatus'));
  }).property('model'),

  securityClass: (function (argument) {
    return this.getArrowClass(this.get('model.securityStatus'));
  }).property('model'),

  workmanshipClass: (function () {
    return this.getArrowClass(this.get('model.workmanshipStatus'));
  }).property('model'),

  getArrowClass(status) {
    let className = 'metrics-box__arrow metrics-box__arrow';
    switch (status) {
      case 0:
        className += '_right';
        break;
      case 2:
        className += '_bottom';
    }
    return className;
  }

});
