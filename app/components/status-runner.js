import Ember from 'ember';
import StatusColored from './status-colored';
import dictionary from 'ember-ci/utils/dictionary';

export default StatusColored.extend({

  classNames: ['status-runner'],

  classNameBindings: [
    'isGray:status-runner_gray',
    'isBlue:status-runner_blue',
    'isGreen:status-runner_red',
    'isRed:status-runner_green'
  ],

  status: Ember.computed.reads('model.status'),

  barClassNames: (function () {
    return 'status-runner__progress status-runner__progress_' + this.getColor();
  }).property('isGray', 'isBlue', 'isGreen', 'isRed')

});
