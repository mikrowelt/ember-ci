import Ember from 'ember';
import StatusColored from './status-colored';
import dictionary from 'ember-ci/utils/dictionary';

export default StatusColored.extend({

  target: null,

  isExpanded: false,

  classNameBindings: [
    'isGray:target-table__item_gray',
    'isBlue:target-table__item_blue',
    'isGreen:target-table__item_red',
    'isRed:target-table__item_green',
    'isExpanded:target-table__item_expanded'
  ],

  status: Ember.computed.reads('target.currentBuild.status'),

  buildDate: (function () {
    return this.get('target.currentBuild.startedAt').toString('d/mm/yyyy hh:mmtt');
  }).property('target'),

  buildStatus: (function () {
    let type = this.get('target.type');
    let status = this.get('target.currentBuild.status');
    return dictionary[type + 'Status'][status];
  }).property('target'),

  shouldClose: (function () {
    if (this.get('isExpanded') && this.get('openedItem') !== this.toString()) {
      this.set('isExpanded', false);
    }
  }).observes('openedItem'),

  iconClass: (function () {
    let type = this.get('target.type');
    let iconClass = 'target-table__item-icon';
    if (type === 'build') {
      iconClass += ' fa fa-desktop';
    }
    if (type === 'firewall') {
      iconClass += ' fa fa-fire ';
    }
    return iconClass;
  }).property('target'),

  click () {
    let newState = !this.get('isExpanded');
    this.set('isExpanded', newState);
    if (newState) {
      this.set('openedItem', this.toString());
    }
  }

});
