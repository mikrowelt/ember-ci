import Ember from 'ember';
import dictionary from 'ember-ci/utils/dictionary';

export default Ember.Component.extend({

  isGray: (function () {
    return this.get('status') === 0;
  }).property('target'),

  isBlue: (function () {
    return this.get('status') === 1;
  }).property('target'),

  isGreen: (function () {
    return this.get('status') === 2;
  }).property('target'),

  isRed: (function () {
    return this.get('status') === 3;
  }).property('target'),

  getColor () {
    let colorClass;
    ['isGray', 'isBlue', 'isGreen', 'isRed'].forEach( (colorBool) => {
      let isColored = this.get(colorBool);
      if (isColored) {
        colorClass = dictionary.colorClasses[colorBool]
        return false;
      }
    });
    return colorClass;
  }

});
