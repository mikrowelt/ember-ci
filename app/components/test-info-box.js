import Ember from 'ember';
import InfoBox from './info-box';
import dictionary from 'ember-ci/utils/dictionary';

export default InfoBox.extend({

  infoTemplate: 'info-boxes/test-info-box',

  title: 'Tests',

  testPercent: (function () {
    let passed = this.get('model.passedTests');
    let failed = this.get('model.failedTests');
    let sum = passed + failed;
    return (passed / sum * 100).toFixed(0);
  }).property('model'),

  testTextStatus: (function () {
    return dictionary.buildInfoStatus[this.get('model.status')];
  }).property('model'),

  textColorClass: (function () {
    return 'test-info-box__chart-info-percent test-info-box__chart-info-percent_' + this.getColor();
  }).property('model'),

  coverageProgress: (function () {
    return new Ember.Handlebars.SafeString('width:' + this.get('model.codeCoverage') + '%;');
  }).property('model'),

  chartData: (function () {
    return [
      {
        label: 'Passed Tests',
        value: this.get('model.passedTests'),
        color: 'green'
      },
      {
        label: 'Failed Tests',
        value: this.get('model.failedTests'),
        color: 'red'
      }
    ];
  }).property('model'),

  chartOptions: (function () {
    return {showTooltips: false};
  }).property('model')

});
