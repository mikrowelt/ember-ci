import DS from 'ember-data';

export default DS.Model.extend({

  title: DS.attr(),

  owner: DS.attr(),

  type: DS.attr(),

  currentBuild: DS.belongsTo('build')

});
