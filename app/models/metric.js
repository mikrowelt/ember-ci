import DS from 'ember-data';

export default DS.Model.extend({

  build: DS.belongsTo('build'),

  testValue: DS.attr(),

  testStatus: DS.attr(),

  securityValue: DS.attr(),

  securityStatus: DS.attr(),

  maintainabilityValue: DS.attr(),

  maintainabilityStatus: DS.attr(),

  workmanshipValue: DS.attr(),

  workmanshipStatus: DS.attr(),

  status: DS.attr()//failed, success, running, pending

});
