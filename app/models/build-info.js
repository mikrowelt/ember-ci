import DS from 'ember-data';

export default DS.Model.extend({

  build: DS.belongsTo('build'),

  buildAt: DS.attr('date'),

  status: DS.attr()//failed, success, running, pending

});
