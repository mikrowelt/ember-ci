import DS from 'ember-data';

export default DS.Model.extend({

  type: DS.attr(),

  passedTests: DS.attr(),

  failedTests: DS.attr(),

  codeCoverage: DS.attr(),

  status: DS.attr(),//failed, success, running, pending

  build: DS.belongsTo('build', { inverse: 'unitTest' })

});
