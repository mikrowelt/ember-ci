import DS from 'ember-data';

export default DS.Model.extend({

  target: DS.belongsTo('target'),

  startedAt: DS.attr('date'),

  status: DS.attr(),

  metric: DS.belongsTo('metric'),

  buildInfo: DS.belongsTo('buildInfo'),

  unitTest: DS.belongsTo('testInfo', { inverse: null }),

  functionalTest: DS.belongsTo('testInfo')

});
